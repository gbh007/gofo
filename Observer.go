package gofo

import (
	"os"
	"sync"
	"time"
)

// Observer структура для наблюдения за файлом
type Observer struct {
	TimeInterval time.Duration
	Code         string
	Path         string
	Log          interface{ Error(v ...interface{}) bool } // интерфейс для вывода ошибок
	C            chan *Message                             // канал для отправки сообщений об изменении поступления данных
	cnt          chan oKey                                 // канал для управления наблюдателем
	holeStatus   bool                                      // имеет значение истины если один из чекеров сработал (после сброса устанавливается в ложь)
	LastUpdate   *time.Time
	Checkers     []time.Duration
	checkersMap  map[time.Duration]bool
	mutex        sync.Mutex
}

// NewObserver создает нового наблюдателя
func NewObserver(code, path string, checkers []time.Duration) *Observer {
	return &Observer{
		TimeInterval: time.Second * 10,
		Code:         code,
		Path:         path,
		Log:          &Log{},
		C:            make(chan *Message, 100),
		cnt:          make(chan oKey),
		Checkers:     checkers,
	}
}

// Run запускает наблюдение за файлом
func (o *Observer) Run() {
	o.mutex.Lock()
	defer o.mutex.Unlock()
	o.LastUpdate = o.check()
	run := true
	ticker := time.NewTicker(o.TimeInterval)
	for {
		select {
		case com := <-o.cnt:
			switch com {
			case oKill:
				return
			case oStop:
				run = false
			case oStart:
				run = true
			}
		case <-ticker.C:
			if !run {
				continue
			}
			lu := o.check()
			if lu == nil {
				continue
			}
			o.runCheckers(lu)
			o.LastUpdate = lu
		}
	}
}

// clearCheckers отчишает карту чекеров
func (o *Observer) clearCheckers() {
	if o.checkersMap == nil {
		o.checkersMap = make(map[time.Duration]bool)
	}
	for _, checker := range o.Checkers {
		o.checkersMap[checker] = false
	}
	o.holeStatus = false
}

// runCheckers запускает проверку на чекеров
func (o *Observer) runCheckers(lu *time.Time) {
	if o.LastUpdate == nil {
		return
	}
	if o.checkersMap == nil {
		o.clearCheckers()
	}
	if lu.After(*o.LastUpdate) {
		if o.holeStatus {
			o.C <- &Message{
				Code:       o.Code,
				Delta:      lu.Sub(*o.LastUpdate),
				LastUpdate: *lu,
				Lost:       false,
			}
		}
		o.clearCheckers()
		return
	}
	for _, checker := range o.Checkers {
		if val, ok := o.checkersMap[checker]; val && ok {
			continue
		}
		if time.Now().Sub(*lu) > checker {
			o.checkersMap[checker] = true
			o.holeStatus = true
			o.C <- &Message{
				Code:       o.Code,
				Delta:      checker,
				LastUpdate: *lu,
				Lost:       true,
			}
		}
	}
}

// check возврашает последнии данные о файле
func (o *Observer) check() *time.Time {
	stat, err := os.Lstat(o.Path)
	if err != nil && o.Log != nil {
		o.Log.Error(err)
		return nil
	}
	lu := stat.ModTime()
	return &lu
}

// Stop останавливает мониторинг
func (o *Observer) Stop() { o.cnt <- oStop }

// Start запускает мониторинг
func (o *Observer) Start() { o.cnt <- oStart }

// Kill убивает мониторинг
func (o *Observer) Kill() { o.cnt <- oKill }

// GetDelta возвращает время с последнего изменения файла
func (o *Observer) GetDelta() time.Duration {
	lu := o.check()
	if lu == nil {
		return -1
	}
	return time.Now().Sub(*lu)
}
