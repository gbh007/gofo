package gofo

import "log"

type oKey int

const (
	// TimeFormat стандартный формат времени для форматирования сообщений
	TimeFormat      = "2006-01-02 15:04:05"
	oKill      oKey = iota
	oStop
	oStart
)

// Log тип для логирования ошибок в стандартный поток
type Log struct{}

func (l *Log) Error(v ...interface{}) bool {
	log.Println(v...)
	return true
}
