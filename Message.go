package gofo

import "time"

// Message структура для сообщений наблюдателя
type Message struct {
	Code       string
	Delta      time.Duration
	Lost       bool // истина если потеря сигнала, ложь если востановление
	LastUpdate time.Time
}

func (m *Message) String() string {
	res := m.LastUpdate.String()
	if m.Lost {
		res += " - " + m.Code
	} else {
		res += " + " + m.Code
	}
	res += " " + m.Delta.String()
	return res
}

// MessageJSON структура для сообщений наблюдателя в формате JSON
type MessageJSON struct {
	Code       string `json:"code"`
	Delta      int64  `json:"delta"`
	Lost       bool   `json:"lost"`
	LastUpdate string `json:"last_update"`
}

// Load загружает данные из сообщение (конвертация)
func (m *MessageJSON) Load(mess *Message) *MessageJSON {
	m.Code = mess.Code
	// перевод в секунды,
	// в функции возвращающей количество секунд delta.Seconds()
	// этот же метод только с доп мусором
	m.Delta = int64(mess.Delta / time.Second)
	m.Lost = mess.Lost
	m.LastUpdate = mess.LastUpdate.Format(TimeFormat)
	return m
}

// Get конвертирует в стандартную структуру
func (m *MessageJSON) Get() (*Message, error) {
	mess := &Message{}
	var err error
	mess.Code = m.Code
	mess.Delta = time.Duration(m.Delta) * time.Second
	mess.Lost = m.Lost
	mess.LastUpdate, err = time.Parse(TimeFormat, m.LastUpdate)
	return mess, err
}
